import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ShowComponent } from './Ad/show/show.component';
import { AddComponent } from './Ad/add/add.component';
import { EditComponent } from './Ad/edit/edit.component';
import { FormsModule } from  '@angular/forms';
import { ServiceAdService } from '../app/Service/service-ad.service';
import { HttpClientModule } from '@angular/common/http';
import { LoginAdminComponent } from './Login/login-admin/login-admin.component';
import { LoginSellerComponent } from './Login/login-seller/login-seller.component';
import { AddpComponent } from './Phone/addp/addp.component';
import { EditpComponent } from './Phone/editp/editp.component';
import { ShowpComponent } from './Phone/showp/showp.component';
import { AddsComponent } from './Seller/adds/adds.component';
import { EditsComponent } from './Seller/edits/edits.component';
import { ShowsComponent } from './Seller/shows/shows.component';
import { HomeComponent } from './Homes/home/home.component';
import { MainAdminComponent } from './Homes/main-admin/main-admin.component';
import { MainSellComponent } from './Homes/main-sell/main-sell.component';
import { ServicePhoneService } from '../app/Service/service-phone.service';
import { AddProductComponent } from './Product/add-product/add-product.component';
import { EditProductComponent } from './Product/edit-product/edit-product.component';
import { ShowProductComponent } from './Product/show-product/show-product.component';
import { ServiceProductService } from './Service/service-product.service';
import { ProfileComponent } from './Homes/profile/profile.component';
import { ShowRequestComponent } from './Request/show-request/show-request.component';
import { EditRequestComponent } from './Request/edit-request/edit-request.component';
import { AddRequestComponent } from './Request/add-request/add-request.component';
import { ServiceRequestService } from '../app/Service/service-request.service';
import { ServiceProducthasrequestService } from '../app/Service/service-producthasrequest.service';
import { ProductAdminComponent } from './Product/product-admin/product-admin.component';
import { ProductSellComponent } from './Product/product-sell/product-sell.component';
import { ProfileSellComponent } from './Homes/profile-sell/profile-sell.component';
import { ShowRequestSellComponent } from './Request/show-request-sell/show-request-sell.component';
import { EditpSellComponent } from './Phone/editp-sell/editp-sell.component';
import { AddpSellComponent } from './Phone/addp-sell/addp-sell.component';
import { ShowpSellComponent } from './Phone/showp-sell/showp-sell.component';
import { HomeCarComponent } from './Car/home-car/home-car.component';
import { CarAddProductsComponent } from './Car/car-add-products/car-add-products.component';
import { EditCarComponent } from './Car/edit-car/edit-car.component';

@NgModule({
  declarations: [
    AppComponent,
    ShowComponent,
    AddComponent,
    EditComponent,
    LoginAdminComponent,
    LoginSellerComponent,
    AddpComponent,
    EditpComponent,
    ShowpComponent,
    AddsComponent,
    EditsComponent,
    ShowsComponent,
    HomeComponent,
    MainAdminComponent,
    MainSellComponent,
    AddProductComponent,
    EditProductComponent,
    ShowProductComponent,
    ProfileComponent,
    ShowRequestComponent,
    EditRequestComponent,
    AddRequestComponent,
    ProductAdminComponent,
    ProductSellComponent,
    ProfileSellComponent,
    ShowRequestSellComponent,
    EditpSellComponent,
    AddpSellComponent,
    ShowpSellComponent,
    HomeCarComponent,
    CarAddProductsComponent,
    EditCarComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule

  ],
  providers: [ServiceAdService,ServicePhoneService, ServiceProductService, ServiceRequestService, ServiceProducthasrequestService],
  bootstrap: [AppComponent]
})
export class AppModule { }
