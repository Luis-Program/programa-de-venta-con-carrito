import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Product } from '../Model/Product';

@Injectable({
  providedIn: 'root'
})
export class ServiceProductService {

  constructor(private http:HttpClient) { }

  Url='http://localhost:8080/Product';

    getProducts(){
     return this.http.get<Product[]>(this.Url);
    }

    createProduct(product:Product){
      return this.http.post<Product>(this.Url,product);
    }

    getProductId(idproduct:number){
      return this.http.get<Product>(this.Url+"/"+idproduct);
    }

    updateProduct(product:Product){
      return this.http.put<Product>(this.Url+"/"+product.idproduct,product);
    }

    deleteProduct(product:Product){
      return this.http.delete<Product>(this.Url+"/"+product.idproduct);
    }

}
