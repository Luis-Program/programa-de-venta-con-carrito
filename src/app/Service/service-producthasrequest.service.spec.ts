import { TestBed } from '@angular/core/testing';

import { ServiceProducthasrequestService } from './service-producthasrequest.service';

describe('ServiceProducthasrequestService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ServiceProducthasrequestService = TestBed.get(ServiceProducthasrequestService);
    expect(service).toBeTruthy();
  });
});
