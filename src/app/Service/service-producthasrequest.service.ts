import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Product_Has_Request } from '../Model/Product_Has_Request';

@Injectable({
  providedIn: 'root'
})
export class ServiceProducthasrequestService {

  constructor(private http:HttpClient) { }

  private Url = "http://localhost:8080/Product_Has_Request";

  getOrderByRequest(id:number){
    return this.http.get<Product_Has_Request[]>(this.Url+"/requestidrequest/"+id); 
  }
  createOrder(order:Product_Has_Request){
    return this.http.post<Product_Has_Request>(this.Url, order);
  }
  getOrderId(id:number){
    return this.http.get<Product_Has_Request>(this.Url+"/"+id);
  }
  updateOrder(order:Product_Has_Request){
    return this.http.put<Product_Has_Request>(this.Url+"/"+order.idproducthasrequest,order);
  }
  deleteOrder(order:Product_Has_Request){
    return this.http.delete<Product_Has_Request>(this.Url+"/"+order.idproducthasrequest);
  }
}
