import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Phone } from '../Model/Phone';

@Injectable({
  providedIn: 'root'
})
export class ServicePhoneService {  
  
  constructor(private http:HttpClient) { }
  
  private Url="http://localhost:8080/Phone";

 
  getSellPhones(id:number){
    return this.http.get<Phone[]>(this.Url+"/selleridseller/"+id);
  }
  createPhone(phone:Phone){
    return this.http.post<Phone>(this.Url,phone);
  }
  getPhoneId(id:number){
    return this.http.get<Phone>(this.Url+"/"+id);
  }
  updatePhone(phone:Phone){
    return this.http.put<Phone>(this.Url+"/"+phone.idphone,phone);
  }
  deletePhone(phone:Phone){
    return this.http.delete<Phone>(this.Url+"/"+phone.idphone);
  }
}
