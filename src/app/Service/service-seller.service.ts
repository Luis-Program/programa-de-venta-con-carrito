import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Seller } from '../Model/Seller';

@Injectable({
  providedIn: 'root'
})
export class ServiceSellerService {

  constructor(private http:HttpClient) { }
  Url='http://localhost:8080/Seller';

  getSellers(){
    return this.http.get<Seller[]>(this.Url);
  }
  createSell(sell:Seller){
    return this.http.post<Seller>(this.Url,sell);
  }
  getSellId(idseller:number){
    return this.http.get<Seller>(this.Url+"/"+idseller);
  }
  updateSell(sell:Seller){
    return this.http.put<Seller>(this.Url+"/"+sell.idseller,sell);
  }
  deleteSell(sell:Seller){
    return this.http.delete<Seller>(this.Url+"/"+sell.idseller);
  }
}
