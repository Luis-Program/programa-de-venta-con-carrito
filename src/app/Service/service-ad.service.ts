import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Ad } from '../Model/Ad';

@Injectable({
  providedIn: 'root'
})
export class ServiceAdService {
  
    
  constructor(private http:HttpClient) { }

  Url='http://localhost:8080/Ad';
  

  getAds(){
    return this.http.get<Ad[]>(this.Url);
  }

  createAd(ad:Ad){
    return this.http.post<Ad>(this.Url,ad);
  }

  getAdId(idad:number){

    return this.http.get<Ad>(this.Url+"/"+idad);

  }

  updateAd(ad:Ad){

    return this.http.put<Ad>(this.Url+"/"+ad.idad,ad);
  }

  deleteAd(ad:Ad){
    return this.http.delete<Ad>(this.Url+"/"+ad.idad);
  }

}
