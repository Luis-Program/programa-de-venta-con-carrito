import { TestBed } from '@angular/core/testing';

import { ServiceAdService } from './service-ad.service';

describe('ServiceAdService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ServiceAdService = TestBed.get(ServiceAdService);
    expect(service).toBeTruthy();
  });
});
