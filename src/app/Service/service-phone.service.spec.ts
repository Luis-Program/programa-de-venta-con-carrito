import { TestBed } from '@angular/core/testing';

import { ServicePhoneService } from './service-phone.service';

describe('ServicePhoneService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ServicePhoneService = TestBed.get(ServicePhoneService);
    expect(service).toBeTruthy();
  });
});
