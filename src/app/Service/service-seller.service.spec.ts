import { TestBed } from '@angular/core/testing';

import { ServiceSellerService } from './service-seller.service';

describe('ServiceSellerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ServiceSellerService = TestBed.get(ServiceSellerService);
    expect(service).toBeTruthy();
  });
});
