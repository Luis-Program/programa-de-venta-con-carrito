import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Request } from '../Model/Request';

@Injectable({
  providedIn: 'root'
})
export class ServiceRequestService {

  constructor(private http:HttpClient) { }

  private Url="http://localhost:8080/Request";

  getRequestByIdSell(id:number){
    return this.http.get<Request[]>(this.Url+"/sellidseller/"+id);
  }
  createRequest(request:Request){
    return this.http.post<Request>(this.Url,request);
  }

  getRequestId(id:number){
    return this.http.get<Request>(this.Url+"/"+id);
  }

  updateRequest(request:Request){
    return this.http.put<Request>(this.Url+"/"+request.idrequest,request);
  }

  deleteRequest(request:Request){
    return this.http.delete<Request>(this.Url+"/"+request.idrequest);
  }
} 
