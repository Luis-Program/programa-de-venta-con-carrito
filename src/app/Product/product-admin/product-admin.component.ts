import { Component, OnInit } from '@angular/core';
import {  Router } from '@angular/router';
import { ServiceProducthasrequestService } from 'src/app/Service/service-producthasrequest.service';
import { Product_Has_Request } from 'src/app/Model/Product_Has_Request';
import { Product } from 'src/app/Model/Product';
import { ServiceProductService } from 'src/app/Service/service-product.service';
import { ServiceRequestService } from 'src/app/Service/service-request.service';
import { Request } from 'src/app/Model/Request';

@Component({  
  selector: 'app-product-admin',
  templateUrl: './product-admin.component.html',
  styleUrls: ['./product-admin.component.css']
})
export class ProductAdminComponent implements OnInit {

  private reqid:number;
  private order: Product_Has_Request[];
  private products: Product[];
  private req: Request = new Request();
  private Cost:number;
  constructor( private service:ServiceProducthasrequestService, private serviceproduct:ServiceProductService, private router:Router, private servicerequest:ServiceRequestService) { }

  ngOnInit() {
    this.Cost = 0;
    this.order = [];
    this.products = []; 

    let r = localStorage.getItem("idreq");
    this.reqid = +r;
    
    this.cargarrequest();

  } 

  cargarrequest(){
    this.servicerequest.getRequestId(this.reqid)
    .subscribe(data=>{
      this.req = data;     this.loadarray(this.req.idrequest);
      })
       
  }

  loadarray(id:number){
    this.service.getOrderByRequest(this.reqid)
    .subscribe(data=>{
      this.order = data; 
      
      if(this.order != null){
          this.load(this.order); 
          for (let i = 0; i  < this.order.length;i++) {
            this.Cost += this.order[i].total;
          }    }
    })
  }
  back(){
    localStorage.setItem("id",this.req.sellidseller.toString());
    this.router.navigate(["show-request"]);
  }
  
  load(array:Array<Product_Has_Request>){

    for (let index = 0; index < array.length; index++) {
      this.serviceproduct.getProductId(array[index].productidproduct)
      .subscribe(data=>{
        this.Vector(data);
      });
    }
  }


  Vector(p:Product){
    this.products.push(p);
  }


}
