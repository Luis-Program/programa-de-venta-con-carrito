import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServiceProductService } from 'src/app/Service/service-product.service';
import { Product } from 'src/app/Model/Product';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.css']
})
export class AddProductComponent implements OnInit {

  private LoginEmpty = false;
  private emptyMessage = "Empty Fields";
  constructor(private router:Router, private service:ServiceProductService) { }

  product:Product = new Product();
  ngOnInit() {
  }

  save(name:string,description:string,cost:number){


    if(name!=""&&description!=""&&cost>0){
      this.LoginEmpty = false;
      this.product.productname = name;
      this.product.productdescription = description;
      this.product.productunitcost = cost;

      this.service.createProduct(this.product)
      .subscribe(data=>{
        this.router.navigate(["show-product"]);
        })
      }else{

        this.LoginEmpty = true;
      }
    
  }

  back(){
    this.router.navigate(["main-admin"]);
  }

}
