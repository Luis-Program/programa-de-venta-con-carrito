import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServiceProductService } from 'src/app/Service/service-product.service';
import { Product } from 'src/app/Model/Product';

@Component({
  selector: 'app-show-product',
  templateUrl: './show-product.component.html',
  styleUrls: ['./show-product.component.css']
})
export class ShowProductComponent implements OnInit {

  products:Product[];
  constructor(private router:Router, private service:ServiceProductService) { }

  ngOnInit() {
    this.getProducts();
  }

  getProducts(){
    this.service.getProducts()
    .subscribe(data=>{
      this.products = data;
    })
  }

  back(){
    this.router.navigate(["main-admin"]);
  }

  delete(p:Product){
    this.service.deleteProduct(p)
    .subscribe(data=>{
      this.products = this.products.filter(pr=>pr!==p)
    })
  }
  edit(p:Product){
    localStorage.setItem("id",p.idproduct.toString());
    this.router.navigate(["edit-product"]);

  }
}
