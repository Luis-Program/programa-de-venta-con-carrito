import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServiceProductService } from 'src/app/Service/service-product.service';
import { Product } from 'src/app/Model/Product';

@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.css']
})
export class EditProductComponent implements OnInit {

  produc:Product = new Product();
  constructor(private router:Router, private service:ServiceProductService) { }

  ngOnInit() {
    this.edit();
  }

  edit(){
    let id = localStorage.getItem("id");
    this.service.getProductId(+id)
    .subscribe(data=>{
      this.produc = data;
    })
  }

  update(product:Product){
    this.service.updateProduct(product)
    .subscribe(data=>{
      this.produc = data;
      this.router.navigate(["show-product"]);
    })

  }
}
