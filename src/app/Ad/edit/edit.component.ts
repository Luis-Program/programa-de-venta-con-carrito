import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServiceAdService } from 'src/app/Service/service-ad.service';
import { Ad } from 'src/app/Model/Ad';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

  ad :Ad = new Ad();
  constructor(private router:Router,private service:ServiceAdService) { }

  ngOnInit() {
    this.edit();  
  } 

  edit(){
    let id=localStorage.getItem("id");
    this.service.getAdId(+id)
    .subscribe(data=>
      {
        this.ad = data;
      })
  } 
  
  update(ad:Ad){

    this.service.updateAd(ad)
    .subscribe(data=>{
      this.ad = data;
      this.router.navigate(["show"]);
    })
  }
}
