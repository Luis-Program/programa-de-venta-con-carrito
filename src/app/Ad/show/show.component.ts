import { Component, OnInit } from '@angular/core';
import { ServiceAdService } from 'src/app/Service/service-ad.service';
import { Router } from '@angular/router';
import { Ad } from 'src/app/Model/Ad';

@Component({
  selector: 'app-show',
  templateUrl: './show.component.html',
  styleUrls: ['./show.component.css']
})
export class ShowComponent implements OnInit {

  ads:Ad[];
  constructor(private service:ServiceAdService, private router:Router) { }

  ngOnInit() {

    this.service.getAds() 
    .subscribe(data=>{
      this.ads=data;
    })
  }
  
  edit(ad:Ad):void{
    localStorage.setItem("id",ad.idad.toString());
    this.router.navigate(["edit"]);
  }

  delete(ad:Ad){
    this.service.deleteAd(ad)
    .subscribe(data=>{
      this.ads=this.ads.filter(a=>a!==ad);
    })
  }
  back(){ 
    this.router.navigate(["main-admin"]);
  }
}
