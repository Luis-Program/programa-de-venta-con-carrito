import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServiceAdService } from 'src/app/Service/service-ad.service';
import { Ad } from 'src/app/Model/Ad';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {

  ad1:Ad = new Ad();
  constructor(private router:Router, private service:ServiceAdService) { }

  ngOnInit() {
  } 
  
  save(adtext:string){
    this.ad1.adtext = adtext;
    this.service.createAd(this.ad1)
    .subscribe(data=>{
      this.router.navigate(["show"]);})   }

  back(){
    this.router.navigate(["main-admin"]); }

    
}
