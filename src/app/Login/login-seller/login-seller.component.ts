import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Seller } from 'src/app/Model/Seller';
import { ServiceSellerService } from 'src/app/Service/service-seller.service';

@Component({
  selector: 'app-login-seller',
  templateUrl: './login-seller.component.html',
  styleUrls: ['./login-seller.component.css']
})
export class LoginSellerComponent implements OnInit {

  username: string;
  password : string;
  passMessage: string;
  errorMessage  : string;
  emptyMessage : string;
  invalidLogin = false;
  pass = false;
  loginEmpty = false;
  Sell:Seller = new Seller();

  Sells: Seller[];
  constructor(private router:Router, private service:ServiceSellerService) { }

  ngOnInit() {
    this.Sells = [];

    this.service.getSellers()
    .subscribe(data=>{
      this.Sells = data;
    })
  }


  handleLogin(username:number,password:string) {
    this.Sell = null;
    if(username != null && password != null ){
      if(this.find(username)!=null){
          this.loginEmpty = false;
          this.invalidLogin = false;
          if(this.Sell.idseller == username && this.Sell.sellerpassword == password){
            this.pass = false;
            if(this.Sell.sellercondition == "active" || this.Sell.sellercondition == "Active"){
              this.invalidLogin = false;
              localStorage.setItem("id",this.Sell.idseller.toString());
             this.gohome(this.Sell.idseller);
            }else{
              this.invalidLogin = true;
              this.errorMessage = 'Account Inactive';}
            }else{
            this.pass = true;
            this.passMessage = "Password Incorrect";}
      }else{this.invalidLogin = true; this.errorMessage='Id non-existent'; this.loginEmpty = false; }
  }else{
      this.loginEmpty = true;
      this.pass = false;
      this.emptyMessage = 'Empty Fields';}       
  }

  gohome(id:number){
    this.router.navigate(["main-sell"]);
  }
  

  find(id:number){

    for (let index = 0; index < this.Sells.length; index++) {
      if (this.Sells[index].idseller == id) {
        this.Sell = this.Sells[index];
        return this.Sell;
        break;}} 
    return null;
  }
}
