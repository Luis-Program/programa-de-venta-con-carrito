import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login-admin',
  templateUrl: './login-admin.component.html',
  styleUrls: ['./login-admin.component.css']
})
export class LoginAdminComponent implements OnInit {
  username: string;
  password : string;
  successMessage: string;
  errorMessage  : string; 
  emptyMessage : string;
  loginEmpty = false;
  invalidLogin = false;
  loginSuccess = false;
  

  constructor(private router: Router) { }
  
  ngOnInit() {
  }

  handleLogin(username,password) {
    
    if(username != null && password != null ){
    if(username=="admin" && password=="admin"){
     this.loginSuccess=true;
     this.invalidLogin=false;
     this.loginEmpty=false;
     this.successMessage='Login Succesful';
     this.router.navigate(["main-admin"]);
      
    }else{
    this.loginSuccess=false;
    this.invalidLogin=true;
    this.loginEmpty=false;
    this.errorMessage='Login Unsuccesful';
    }
  }else{
      this.loginEmpty = true;
      this.emptyMessage = 'Empty Fields';
  }
         
  }
}
