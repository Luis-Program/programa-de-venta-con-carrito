import { Component, OnInit } from '@angular/core';
import { Seller } from 'src/app/Model/Seller';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { ServiceSellerService } from 'src/app/Service/service-seller.service';

@Component({
  selector: 'app-edits',
  templateUrl: './edits.component.html',
  styleUrls: ['./edits.component.css']
})
export class EditsComponent implements OnInit {

  private sell:Seller = new Seller();
  private confirm: string;
  private conditionMessage = "-.Place active or inactive.-";
  private conditionIncorrect = false;
  private emptyMessage = "Empty Fields";
  private errorMessage = "-.Password: Required data an uppercase, lowercase and a number at least.-";
  private differentMessage = ".-Passwords don´t match-.";
  private LoginEmpty = false;
  private invalidLogin = false; 
  private differentpass = false;  
  private i:number;
  constructor(private service:ServiceSellerService,private router:Router) { }

  ngOnInit() {
    this.edit();  
  }
  edit(){
    let id = localStorage.getItem("id");
    this.service.getSellId(+id)
    .subscribe(data=>{
      this.sell = data;
    })
  }
  update(sell:Seller,pass:string){
    if(sell.sellername!=""&& sell.sellersurname!=""&& sell.sellerbirthdate!=""&& sell.selleremail!=""&&sell.sellerpassword!=""&& sell.sellerdeliveryaddress!=""&& sell.sellercondition!=""&& pass!=""){
      this.LoginEmpty = false;
      if(sell.sellercondition == "active" || sell.sellercondition == "inactive" || sell.sellercondition == "Active" || sell.sellercondition == "Inactive"  ){
        this.conditionIncorrect = false;
      if(this.validate(sell.sellerpassword)){ 
        this.invalidLogin = false;
        if(sell.sellerpassword == pass){
          this.differentpass = false;
              this.service.updateSell(sell)
              .subscribe(data=>{
                this.sell = data;
                this.router.navigate(["shows"]);})
        }else{ this.differentpass = true;  }
      }else{ this.invalidLogin = true; }

    }else{this.conditionIncorrect = true;}

    }else{ this.LoginEmpty = true; }
    }


    validate(tx:string){

      var nMay = 0, nMin = 0, nNum = 0
    var t1 = "ABCDEFGHIJKLMNÑOPQRSTUVWXYZ"
    var t2 = "abcdefghijklmnñopqrstuvwxyz"
    var t3 = "0123456789"
    for (this.i=0;this.i<tx.length;this.i++) {
      if ( t1.indexOf(tx.charAt(this.i)) != -1 ) {nMay++}
      if ( t2.indexOf(tx.charAt(this.i)) != -1 ) {nMin++}
      if ( t3.indexOf(tx.charAt(this.i)) != -1 ) {nNum++}
    }
    if ( nMay>0 && nMin>0 && nNum>0 ) { return true }
    else { return false }
    }

    back(){
      this.router.navigate(["shows"]);
    }
}
