import { Component, OnInit } from '@angular/core';
import { Seller } from 'src/app/Model/Seller';
import { Router } from '@angular/router';
import { ServiceSellerService } from 'src/app/Service/service-seller.service';
import { Phone } from 'src/app/Model/Phone';
import { stringify } from '@angular/compiler/src/util';

@Component({
  selector: 'app-adds',
  templateUrl: './adds.component.html',
  styleUrls: ['./adds.component.css']
})
export class AddsComponent implements OnInit {

  private sell1:Seller = new Seller();
  private repassword:string;
  private conditionMessage = "-.Place active or inactive.-";
  private conditionIncorrect = false;
  private emptyMessage = "Empty Fields";
  private errorMessage = "-.Password: Required data an uppercase, lowercase and a number at least.-";
  private differentMessage = ".-Passwords don´t match-.";
  private LoginEmpty = false;
  private invalidLogin = false; 
  private differentpass = false;
  private i:number;
  constructor(private router:Router, private service:ServiceSellerService) { }
  ngOnInit() {
  }
 
  

save(sellername:string, sellersurname:string,sellerbirthdate:string,selleremail:string, sellerpassword:string,sellerdeliveryaddress:string, sellercondition:string, confirm:string){

  if(sellername!=""&&sellersurname!=""&&sellerbirthdate!=""&&selleremail!=""&&sellerpassword!=""&&sellerdeliveryaddress!=""&&sellercondition!=""&& confirm!=""){
    this.LoginEmpty = false;
      if(sellercondition == "active" || sellercondition == "inactive" || sellercondition == "Active" || sellercondition == "Inactive"  ){
        this.conditionIncorrect = false;
    if(this.validate(sellerpassword)){
      this.invalidLogin = false;
      if(sellerpassword == confirm){
        this.differentpass = false;
        this.sell1.sellername = sellername;
        this.sell1.sellersurname = sellersurname;
        this.sell1.selleremail = selleremail;
        this.sell1.sellerbirthdate = sellerbirthdate;
        this.sell1.sellerpassword = sellerpassword;
        this.sell1.sellerdeliveryaddress = sellerdeliveryaddress;
        this.sell1.sellercondition = sellercondition;
        this.sell1.phones = [];
        this.service.createSell(this.sell1)
                .subscribe(data=>{
                this.router.navigate(["shows"]); })
      }else{ this.differentpass = true;  }
    }else{ this.invalidLogin = true; }

  }else{ this.conditionIncorrect = true; }
  }else{ this.LoginEmpty = true; }  
}

  validate(tx:string){

    var nMay = 0, nMin = 0, nNum = 0
	var t1 = "ABCDEFGHIJKLMNÑOPQRSTUVWXYZ"
	var t2 = "abcdefghijklmnñopqrstuvwxyz"
	var t3 = "0123456789"
	for (this.i=0;this.i<tx.length;this.i++) {
		if ( t1.indexOf(tx.charAt(this.i)) != -1 ) {nMay++}
		if ( t2.indexOf(tx.charAt(this.i)) != -1 ) {nMin++}
		if ( t3.indexOf(tx.charAt(this.i)) != -1 ) {nNum++}
	}
	if ( nMay>0 && nMin>0 && nNum>0 ) { return true }
	else { return false }
  }

  back(){
    this.router.navigate(["main-admin"]);
  }
}
