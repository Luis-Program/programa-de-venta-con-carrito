import { Component, OnInit } from '@angular/core';
import { ServiceSellerService } from 'src/app/Service/service-seller.service';
import { Router } from '@angular/router';
import { Seller } from 'src/app/Model/Seller';

@Component({
  selector: 'app-shows',
  templateUrl: './shows.component.html',
  styleUrls: ['./shows.component.css']
})
export class ShowsComponent implements OnInit {

  Sellers: Seller[];  
  constructor(private service:ServiceSellerService,private router:Router) { }

  ngOnInit() {
    this.service.getSellers() 
    .subscribe(data=>
      {this.Sellers=data; })  
  } 

  req(sell:Seller){
    localStorage.setItem("id",sell.idseller.toString());
    this.router.navigate(["show-request"]);
  }

  back(){ 
    this.router.navigate(["main-admin"]);
  } 

  edit(sell:Seller):void{
    localStorage.setItem("id",sell.idseller.toString());
    this.router.navigate(["edits"]);
  }
  delete(sell:Seller){
    this.service.deleteSell(sell)
    .subscribe(data=>{
      this.Sellers=this.Sellers.filter(s=>s!=sell);
    })
  }
  phone(sell:Seller){
    localStorage.setItem("id",sell.idseller.toString());
    this.router.navigate(["showp"]);

  }
}
