import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddComponent } from './Ad/add/add.component';
import { EditComponent } from './Ad/edit/edit.component';
import { ShowComponent } from './Ad/show/show.component'
import { LoginAdminComponent } from './Login/login-admin/login-admin.component';
import { LoginSellerComponent } from './Login/login-seller/login-seller.component';
import { AddpComponent } from './Phone/addp/addp.component';
import { EditpComponent } from './Phone/editp/editp.component';
import { ShowpComponent } from './Phone/showp/showp.component';
import { AddsComponent } from './Seller/adds/adds.component';
import { EditsComponent } from './Seller/edits/edits.component';
import { ShowsComponent } from './Seller/shows/shows.component';
import { HomeComponent } from './Homes/home/home.component';
import { MainAdminComponent } from './Homes/main-admin/main-admin.component';
import { MainSellComponent } from './Homes/main-sell/main-sell.component';
import { AddProductComponent } from './Product/add-product/add-product.component';
import { EditProductComponent } from './Product/edit-product/edit-product.component';
import { ShowProductComponent } from './Product/show-product/show-product.component';
import { ProfileComponent } from './Homes/profile/profile.component';
import { AddRequestComponent } from './Request/add-request/add-request.component';
import { EditRequestComponent } from './Request/edit-request/edit-request.component';
import { ShowRequestComponent } from './Request/show-request/show-request.component';
import { ProductAdminComponent } from './Product/product-admin/product-admin.component';
import { ProductSellComponent } from './Product/product-sell/product-sell.component';
import { ProfileSellComponent } from './Homes/profile-sell/profile-sell.component';
import { ShowRequestSellComponent } from './Request/show-request-sell/show-request-sell.component';
import { AddpSellComponent } from './Phone/addp-sell/addp-sell.component';
import { EditpSellComponent } from './Phone/editp-sell/editp-sell.component';
import { ShowpSellComponent } from './Phone/showp-sell/showp-sell.component';
import { HomeCarComponent } from './Car/home-car/home-car.component';
import { CarAddProductsComponent } from './Car/car-add-products/car-add-products.component';
import { EditCarComponent } from './Car/edit-car/edit-car.component';



const routes: Routes = [
      {path:'' ,component: HomeComponent},
      {path:'add',component:AddComponent},
      {path:'edit',component:EditComponent},
      {path:'show',component:ShowComponent},
      {path: 'login-admin',component:LoginAdminComponent},
      {path: 'login-seller',component:LoginSellerComponent},
      {path: 'addp',component:AddpComponent},
      {path: 'editp', component:EditpComponent},
      {path: 'showp', component:ShowpComponent},
      {path: 'adds',component: AddsComponent},
      {path: 'edits', component: EditsComponent},
      {path: 'shows', component: ShowsComponent},
      {path: 'main-admin', component:MainAdminComponent},
      {path: 'main-sell', component:MainSellComponent},
      {path: 'add-product', component:AddProductComponent},
      {path: 'edit-product', component:EditProductComponent},
      {path: 'show-product', component:ShowProductComponent},
      {path: 'profile', component: ProfileComponent},
      {path: 'add-request', component:AddRequestComponent},
      {path: 'edit-request', component:EditRequestComponent},
      {path: 'show-request', component:ShowRequestComponent},
      {path: 'show-product', component:ShowProductComponent},
      {path: 'add-product',component:AddProductComponent},
      {path: 'edit-product',component:EditProductComponent},
      {path: 'product-admin', component:ProductAdminComponent},
      {path: 'product-sell', component:ProductSellComponent},
      {path: 'profile-sell', component:ProfileSellComponent},
      {path: 'show-request-sell', component:ShowRequestSellComponent},
      {path: 'addp-sell', component:AddpSellComponent},
      {path: 'editp-sell', component:EditpSellComponent},
      {path: 'showp-sell', component:ShowpSellComponent},
      {path: 'home-car', component:HomeCarComponent},
      {path: 'car-add-products', component: CarAddProductsComponent},
      {path: 'edit-car', component: EditCarComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
