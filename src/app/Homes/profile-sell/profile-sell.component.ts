import { Component, OnInit } from '@angular/core';
import { Seller } from 'src/app/Model/Seller';
import { Router} from '@angular/router';
import { ServiceSellerService } from 'src/app/Service/service-seller.service';

@Component({
  selector: 'app-profile-sell',
  templateUrl: './profile-sell.component.html',
  styleUrls: ['./profile-sell.component.css']
})
export class ProfileSellComponent implements OnInit {

  private sell:Seller = new Seller();
  private confirm: string;
  private emptyMessage = "Empty Fields";
  private errorMessage = "-.Password: Required data an uppercase, lowercase and a number at least.-";
  private differentMessage = ".-Passwords don´t match-.";
  private LoginEmpty = false;
  private invalidLogin = false; 
  private differentpass = false;  
  private i:number;
  private idsell: number;
  constructor(private service:ServiceSellerService,private router:Router) { }

  ngOnInit() {  
    let ids = localStorage.getItem("id");
    this.idsell = +ids;
    this.edit(); 
  }

  edit(){
    this.service.getSellId(this.idsell)
    .subscribe(data=>{
      this.sell = data;
    })
  }

  update(sell:Seller,pass:string){
    if(sell.sellername!=""&& sell.sellersurname!=""&& sell.sellerbirthdate!=""&& sell.selleremail!=""&&sell.sellerpassword!=""&& sell.sellerdeliveryaddress!=""&& sell.sellercondition!=""&& pass!=""){
      this.LoginEmpty = false;
      if(this.validate(sell.sellerpassword)){ 
        this.invalidLogin = false;
        if(sell.sellerpassword == pass){
          this.differentpass = false;
              this.service.updateSell(sell)
              .subscribe(data=>{
                this.sell = data;
                localStorage.setItem("id",this.idsell.toString());
                this.router.navigate(["main-sell"]);})
        }else{ this.differentpass = true;  }
      }else{ this.invalidLogin = true; }
    }else{ this.LoginEmpty = true; }
    }

  back(){
    localStorage.setItem("id",this.idsell.toString());
    this.router.navigate(["main-sell"]);
  }

  validate(tx:string){

    var nMay = 0, nMin = 0, nNum = 0
  var t1 = "ABCDEFGHIJKLMNÑOPQRSTUVWXYZ"
  var t2 = "abcdefghijklmnñopqrstuvwxyz"
  var t3 = "0123456789"
  for (this.i=0;this.i<tx.length;this.i++) {
    if ( t1.indexOf(tx.charAt(this.i)) != -1 ) {nMay++}
    if ( t2.indexOf(tx.charAt(this.i)) != -1 ) {nMin++}
    if ( t3.indexOf(tx.charAt(this.i)) != -1 ) {nNum++}
  }
  if ( nMay>0 && nMin>0 && nNum>0 ) { return true }
  else { return false }
  }
}
