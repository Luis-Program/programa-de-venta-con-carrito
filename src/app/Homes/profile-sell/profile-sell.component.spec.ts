import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileSellComponent } from './profile-sell.component';

describe('ProfileSellComponent', () => {
  let component: ProfileSellComponent;
  let fixture: ComponentFixture<ProfileSellComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfileSellComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileSellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
