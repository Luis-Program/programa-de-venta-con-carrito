import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServiceAdService } from 'src/app/Service/service-ad.service';
import { Ad } from 'src/app/Model/Ad';

@Component({
  selector: 'app-main-sell',
  templateUrl: './main-sell.component.html',
  styleUrls: ['./main-sell.component.css']
})
export class MainSellComponent implements OnInit {

   private idsell: number;
   private ads: Ad[];
  constructor(private router:Router, private service:ServiceAdService) { }

  ngOnInit() {  
    let ids = localStorage.getItem("id");
    this.idsell = +ids;
    this.service.getAds()
    .subscribe(data=>{  
      this.ads = data;
    })
  }

  Logout(){
    this.router.navigate([""]);
  }
  Profile(){
    localStorage.setItem("id",this.idsell.toString());
    this.router.navigate(["profile-sell"]);
  }
  ShowRequest(){
    localStorage.setItem("id",this.idsell.toString());
    this.router.navigate(["show-request-sell"]);
  }
  NewRequest(){
    localStorage.setItem("id",this.idsell.toString());
    this.router.navigate(["add-request"]);
  }

  Phone(){
    localStorage.setItem("id",this.idsell.toString());
  this.router.navigate(["showp-sell"]);
  }

  Cart(){
    this.router.navigate(["home-car"]);
  }


}
