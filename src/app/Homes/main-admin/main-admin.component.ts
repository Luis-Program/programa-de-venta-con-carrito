import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-main-admin',
  templateUrl: './main-admin.component.html',
  styleUrls: ['./main-admin.component.css']
})
export class MainAdminComponent implements OnInit {

  constructor(private router:Router) { }

  ngOnInit() {
  }

  Profile(){
    this.router.navigate(["profile"]);
  }
  Logout(){
    this.router.navigate([""]);
  }
  ShowSell(){
    this.router.navigate(["shows"]);

  }
  NewSell(){
    this.router.navigate(["adds"]);

  }
  ShowProduct(){
    this.router.navigate(["show-product"]);
  }
  NewProduct(){
    this.router.navigate(["add-product"]);
  }
  ShowAd(){
    this.router.navigate(["show"]);
  }
  NewAd(){
    this.router.navigate(["add"]);
  }
}
