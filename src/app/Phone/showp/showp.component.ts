import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Phone } from 'src/app/Model/Phone';
import { ServicePhoneService } from 'src/app/Service/service-phone.service';

@Component({
  selector: 'app-showp',
  templateUrl: './showp.component.html',
  styleUrls: ['./showp.component.css']
})
export class ShowpComponent implements OnInit {

  pho:Phone[];
  ids:number;
  constructor(private router:Router, private service:ServicePhoneService) { }

  ngOnInit() {
    this.pho = [];
    this.getid(); 
    this.ids = parseInt (localStorage.getItem("id"));
  }

  getid(){
   let idsell = parseInt (localStorage.getItem("id"));
   
   this.service.getSellPhones(idsell)
   .subscribe(data=>{
    this.pho = data;
   }) }
  add(){
    localStorage.setItem("idsell",this.ids.toString());
    this.router.navigate(["addp"]);
  }
  back(){
    this.router.navigate(["shows"]);
  }
  edit(phone:Phone):void{
    localStorage.setItem("idpho",phone.idphone.toString());
    this.router.navigate(["editp"]);
  }

  delete(phone:Phone){
    this.service.deletePhone(phone)
    .subscribe(data=>{
      this.pho=this.pho.filter(p=>p!=phone);
    })
  }


}
