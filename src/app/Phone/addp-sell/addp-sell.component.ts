import { Component, OnInit } from '@angular/core';
import { Phone } from 'src/app/Model/Phone';
import { Router} from '@angular/router';
import { ServicePhoneService } from 'src/app/Service/service-phone.service';

@Component({
  selector: 'app-addp-sell',
  templateUrl: './addp-sell.component.html',
  styleUrls: ['./addp-sell.component.css']
})
export class AddpSellComponent implements OnInit {

  private phone:Phone = new Phone();
  private idsell:number;
  constructor(private router:Router, private service:ServicePhoneService) { }

  ngOnInit() {  
    let ids = localStorage.getItem("id");
    this.idsell = +ids;
  }


  save(phonenumber:string, phoneareacode:string){

    this.phone.phonenumber = phonenumber;
    this.phone.phoneareacode = phoneareacode;
    this.phone.selleridseller = this.idsell;
    
    this.service.createPhone(this.phone)
    .subscribe(data=>{
      localStorage.setItem("id",this.idsell.toString());
    this.router.navigate(["showp-sell"]);;
    })

    
  }

}
