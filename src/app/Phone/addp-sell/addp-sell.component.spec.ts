import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddpSellComponent } from './addp-sell.component';

describe('AddpSellComponent', () => {
  let component: AddpSellComponent;
  let fixture: ComponentFixture<AddpSellComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddpSellComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddpSellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
