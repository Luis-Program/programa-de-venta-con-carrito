import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServicePhoneService } from 'src/app/Service/service-phone.service';
import { Phone } from 'src/app/Model/Phone';

@Component({
  selector: 'app-addp',
  templateUrl: './addp.component.html',
  styleUrls: ['./addp.component.css']
})
export class AddpComponent implements OnInit {  

  phone:Phone = new Phone();
  idseller:number;
  constructor(private router:Router, private service:ServicePhoneService) { }

  ngOnInit() {
    this.idseller = parseInt(localStorage.getItem("idsell"));
  }


  save(phonenumber:string, phoneareacode:string){

    this.phone.phonenumber = phonenumber;
    this.phone.phoneareacode = phoneareacode;
    this.phone.selleridseller = this.idseller;
    
    this.service.createPhone(this.phone)
    .subscribe(data=>{
      this.router.navigate(["showp"]);
    })

    
  }

}
