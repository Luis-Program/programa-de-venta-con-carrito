import { Component, OnInit } from '@angular/core';
import { Phone } from 'src/app/Model/Phone';
import { Router } from '@angular/router';
import { ServicePhoneService } from 'src/app/Service/service-phone.service';

@Component({
  selector: 'app-editp',
  templateUrl: './editp.component.html',
  styleUrls: ['./editp.component.css']
})
export class EditpComponent implements OnInit {

  phone: Phone = new Phone();
  constructor(private router:Router, private service:ServicePhoneService) { }

  ngOnInit() {
    this.edit();
  }

  edit(){
    let idp = localStorage.getItem("idpho");
    this.service.getPhoneId(+idp)
    .subscribe(data=>{
      this.phone = data;
    })
  }

  back(){
    this.router.navigate(["showp"])
  }
  update(phone:Phone){
    this.service.updatePhone(phone)
    .subscribe(data=>{
      this.phone = data
    })

    this.router.navigate(["showp"]);
  }
}
