import { Component, OnInit } from '@angular/core';
import {  Router } from '@angular/router';
import { ServicePhoneService } from 'src/app/Service/service-phone.service';
import { Phone } from 'src/app/Model/Phone';

@Component({
  selector: 'app-showp-sell',
  templateUrl: './showp-sell.component.html',
  styleUrls: ['./showp-sell.component.css']
})
export class ShowpSellComponent implements OnInit {

  private pho:Phone[];
  private idsell:number;
  constructor(private router:Router, private service:ServicePhoneService) { }

  ngOnInit() {

    this.load();
    
  }

  load(){
    let ids = localStorage.getItem("id");
    this.idsell = +ids;
    this.search(this.idsell);
  }

  search(id:number){
    this.service.getSellPhones(id)
    .subscribe(data=>{
      this.pho = data;
    })
  }



  back(){
    localStorage.setItem("id",this.idsell.toString());
          this.router.navigate(["main-sell"]);
  }

  delete(phone:Phone){
    this.service.deletePhone(phone)
    .subscribe(data=>{
      this.pho=this.pho.filter(p=>p!=phone);
    })
  }

  edit(phone:Phone):void{
    localStorage.setItem("idp",phone.idphone.toString());
    this.router.navigate(["editp-sell"]);
  }



  add(){
    this.router.navigate(["addp-sell"]);
  }
}
