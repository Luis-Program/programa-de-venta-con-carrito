import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowpSellComponent } from './showp-sell.component';

describe('ShowpSellComponent', () => {
  let component: ShowpSellComponent;
  let fixture: ComponentFixture<ShowpSellComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowpSellComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowpSellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
