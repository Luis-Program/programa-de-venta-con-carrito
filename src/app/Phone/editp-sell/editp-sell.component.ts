import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServicePhoneService } from 'src/app/Service/service-phone.service';
import { Phone } from 'src/app/Model/Phone';

@Component({
  selector: 'app-editp-sell',
  templateUrl: './editp-sell.component.html',
  styleUrls: ['./editp-sell.component.css']
})
export class EditpSellComponent implements OnInit {

  phone: Phone = new Phone();
  private idsell:number;
  constructor(private router:Router, private service:ServicePhoneService) { }

  ngOnInit() {

    this.edit();

  }

  edit(){

    let ids = localStorage.getItem("idp");
    this.idsell = +ids;

    this.service.getPhoneId(this.idsell)
    .subscribe(data=>{
      this.phone = data;
    })
  }

  update(phone:Phone){
    this.service.updatePhone(phone)
    .subscribe(data=>{
      this.phone = data;
    })

    this.back(phone);
  }

  back(phone:Phone){
    localStorage.setItem("id",phone.selleridseller.toString());
    this.router.navigate(["showp-sell"]);
  }

} 
