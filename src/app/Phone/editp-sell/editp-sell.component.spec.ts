import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditpSellComponent } from './editp-sell.component';

describe('EditpSellComponent', () => {
  let component: EditpSellComponent;
  let fixture: ComponentFixture<EditpSellComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditpSellComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditpSellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
