import { Product_Has_Request } from './Product_Has_Request';

export class Request{
    idrequest: number;
    requestname: string;
    requestdate: string;
    sellidseller: number;
    producthasrequest: Product_Has_Request[];
}