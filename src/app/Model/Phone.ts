export class Phone{

    idphone: number;
    phonenumber: string;
    phoneareacode: string;
    selleridseller: number;
}