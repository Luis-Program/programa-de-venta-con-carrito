export class Product{
     idproduct: number;
    productname: string;
    productdescription: string;
    productunitcost: number;
}