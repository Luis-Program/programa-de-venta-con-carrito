import { Phone } from './Phone';

export class Seller{
    idseller: number;
    sellername: string;
    sellersurname: string;
    selleremail: string;
    sellerbirthdate: string;
    sellerpassword: string;
    sellerdeliveryaddress: string;
    sellercondition: string;
    phones: Phone[];
}   