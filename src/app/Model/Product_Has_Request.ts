export class Product_Has_Request{
    idproducthasrequest: number;
    productidproduct: number;
    requestidrequest: number;
    producthasrequestamount: number;
    total: number;
}