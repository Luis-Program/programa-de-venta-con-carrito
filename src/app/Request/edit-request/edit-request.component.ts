import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServiceRequestService } from 'src/app/Service/service-request.service';
import { Request } from 'src/app/Model/Request';

@Component({
  selector: 'app-edit-request',
  templateUrl: './edit-request.component.html',
  styleUrls: ['./edit-request.component.css']
})
export class EditRequestComponent implements OnInit {

  private  idreq: number;
  private req: Request = new Request();
  private idsell:number;
  constructor(private router:Router, private service:ServiceRequestService) { }

  ngOnInit() {
   let id =  localStorage.getItem("idre");
    this.idreq = +id;
    this.service.getRequestId(this.idreq)
    .subscribe(data=>{
      this.req = data;  
      this.idsell = this.req.sellidseller;
    })
    
  }
  back(){
    localStorage.setItem("id",this.idsell.toString());
    this.router.navigate(["show-request-sell"]);
  }

  update(req:Request){
    localStorage.setItem("id",req.sellidseller.toString());
    this.service.updateRequest(req)
    .subscribe(data=>{
      this.req = data;
      this.router.navigate(["show-request-sell"]);
    })
  }

}
