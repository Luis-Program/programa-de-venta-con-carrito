import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowRequestSellComponent } from './show-request-sell.component';

describe('ShowRequestSellComponent', () => {
  let component: ShowRequestSellComponent;
  let fixture: ComponentFixture<ShowRequestSellComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowRequestSellComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowRequestSellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
