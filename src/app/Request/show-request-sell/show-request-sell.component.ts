import { Component, OnInit } from '@angular/core';
import {  Router } from '@angular/router';
import { ServiceRequestService } from 'src/app/Service/service-request.service';
import { Request } from 'src/app/Model/Request';
@Component({
  selector: 'app-show-request-sell',
  templateUrl: './show-request-sell.component.html',
  styleUrls: ['./show-request-sell.component.css']
})
export class ShowRequestSellComponent implements OnInit {

  private idsell: number;
  private Requests: Request[];
  private reqid:number;
  constructor(private router:Router, private service:ServiceRequestService) { }

  ngOnInit() {
    let ids = localStorage.getItem("id");
    this.idsell = +ids;
    this.service.getRequestByIdSell(this.idsell)
    .subscribe(data=>{  
      this.Requests = data;
    })
  }

  back(){
    localStorage.setItem("id",this.idsell.toString());
    this.router.navigate(["main-sell"]);
  }

  delete(req:Request){
    this.service.deleteRequest(req)
    .subscribe(data=>{
      this.Requests = this.Requests.filter(r=>r!==req);
    })
  }

  edit(req:Request){
    localStorage.setItem("idre",req.idrequest.toString());
    this.router.navigate(["edit-request"]); 
  }

  prod(r:Request){
    localStorage.setItem("idre",r.idrequest.toString());
    this.router.navigate(["product-sell"]);
  }


} 
