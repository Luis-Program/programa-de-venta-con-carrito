import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServiceRequestService } from 'src/app/Service/service-request.service';
import { Request } from 'src/app/Model/Request';

@Component({
  selector: 'app-show-request',
  templateUrl: './show-request.component.html',
  styleUrls: ['./show-request.component.css']
})
export class ShowRequestComponent implements OnInit { 
  
  private idsell:number;
  private Requests: Request[];
  private reqid:number;
  constructor( private router:Router, private service:ServiceRequestService) {  }

  
  ngOnInit() {
    this.reqid = 0;
    let id = localStorage.getItem("id");
    this.idsell = +id;
    this.service.getRequestByIdSell(this.idsell)
    .subscribe(data=>{
      this.Requests = data;
    })
    

  }

  back(){
    this.router.navigate(["shows"]);
  }
    

  products(req:Request){
    this.reqid = req.idrequest;
    localStorage.setItem("idreq", this.reqid.toString());
    
    this.router.navigate(["product-admin"]);
  }
  
  
}
