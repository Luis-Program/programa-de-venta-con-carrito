import { Component, OnInit } from '@angular/core';
import { ServiceRequestService } from 'src/app/Service/service-request.service';
import {  Router } from '@angular/router';
import { Request } from 'src/app/Model/Request';

@Component({
  selector: 'app-add-request',
  templateUrl: './add-request.component.html',
  styleUrls: ['./add-request.component.css']
})
export class AddRequestComponent implements OnInit {

  private idsell:number;
  private NewRe:Request = new Request();
  private emptyMessage = "Empty Fields";
  private LoginEmpty = false;
  constructor( private router:Router, private service:ServiceRequestService) { } 

  ngOnInit() {
    let ids = localStorage.getItem("id");
    this.idsell = +ids;
  }

  save(name:string,date:string){
    if (name != "" && date != "") {
      
        this.LoginEmpty = false;

        this.NewRe.requestname = name;
        this.NewRe.requestdate = date;
        this.NewRe.sellidseller = this.idsell;
        this.NewRe.producthasrequest = [];

        this.service.createRequest(this.NewRe)
        .subscribe(data=>{
          localStorage.setItem("id",this.idsell.toString());
          this.router.navigate(["show-request-sell"]);
        })
    }else{
      this.LoginEmpty = true;
    }

  }

  back(){
    localStorage.setItem("id",this.idsell.toString());
          this.router.navigate(["main-sell"]);
  }

}
