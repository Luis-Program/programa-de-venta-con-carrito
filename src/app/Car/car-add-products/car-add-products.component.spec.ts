import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarAddProductsComponent } from './car-add-products.component';

describe('CarAddProductsComponent', () => {
  let component: CarAddProductsComponent;
  let fixture: ComponentFixture<CarAddProductsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarAddProductsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarAddProductsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
