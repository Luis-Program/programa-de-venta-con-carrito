import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/Model/Product';
import { Router } from '@angular/router';
import { ServiceProductService } from 'src/app/Service/service-product.service';
import { Product_Has_Request } from 'src/app/Model/Product_Has_Request';
import { Request } from 'src/app/Model/Request';
import { ServiceProducthasrequestService } from 'src/app/Service/service-producthasrequest.service';
import { ServiceRequestService } from 'src/app/Service/service-request.service';

@Component({
  selector: 'app-car-add-products',
  templateUrl: './car-add-products.component.html',
  styleUrls: ['./car-add-products.component.css']
})
export class CarAddProductsComponent implements OnInit {

  private reqid:number;
  private order: Product_Has_Request[];
  private products: Product[];
  private allpro: Product[];
  private req: Request = new Request();
  private Cost:number;
  private newcar: Product_Has_Request = new Product_Has_Request();
  private emptyMessage : string;
  private loginEmpty = false;
  private error = false;
  private errorMessage : string;
  private p:Product = new Product();
  private cost:string;

  constructor( private service:ServiceProducthasrequestService, private serviceproduct:ServiceProductService, private router:Router, private servicerequest:ServiceRequestService) { } 
  
  ngOnInit() {
    this.p = null;
    this.allpro = [];
    this.loadallproducts();
    this.Cost = 0;
    this.order = [];
    this.products = [];
    this.req = null;
    let id =  localStorage.getItem("idre");
    this.reqid = +id;
    this.cargarrequest();
  } 

  create(id:number,cant:number){
    if(id>0 && cant>0){
      this.loginEmpty = false;
      this.p = this.findproduct(id);
      if(this.p!=null){
        this.error = false;
        this.newcar.productidproduct = id;
        this.newcar.requestidrequest = this.reqid;
        this.newcar.producthasrequestamount = cant;
        this.newcar.total = this.p.productunitcost * cant;
        this.service.createOrder(this.newcar)
        .subscribe(data=>{
          this.loadarray(this.req.idrequest);
        })

      }else{
        this.error = true;
        this.errorMessage = "Non-existent product";
      }

    }else{
      this.loginEmpty = true;
      this.emptyMessage = 'Empty Fields';
    }
  }

  findproduct(id:number){
    this.p = null;
    for (let ind = 0; ind < this.allpro.length; ind++) {
      if(id == this.allpro[ind].idproduct){
        return this.allpro[ind];
        break;
      }}
  }


  loadallproducts(){
    this.serviceproduct.getProducts()
    .subscribe(data=>{
      this.allpro = data;})}  

  cargarrequest(){
    this.servicerequest.getRequestId(this.reqid)
    .subscribe(data=>{
      this.req = data;   
      
      this.loadarray(this.req.idrequest);})}

  loadarray(id:number){
    this.Cost = 0;
    this.service.getOrderByRequest(this.reqid)
    .subscribe(data=>{
      this.order = data; 
      if(this.order != null){
          this.load(this.order);   
        for (let i = 0; i  < this.order.length;i++) {
          this.Cost += this.order[i].total;}
         this.cost = this.Cost.toFixed(2);
        }})
        
        }


  load(array:Array<Product_Has_Request>){
    for (let index = 0; index < array.length; index++) {
      this.serviceproduct.getProductId(array[index].productidproduct)
      .subscribe(data=>{
        this.Vector(data); });}}

  Vector(p:Product){
    this.products.push(p);}


  back(){
    localStorage.setItem("id",this.req.sellidseller.toString());
    this.router.navigate(["home-car"]);
    
  }





  delete(ph:Product_Has_Request){
    this.service.deleteOrder(ph)
    .subscribe(data=>{
      this.order = this.order.filter(r=>r!=ph);
      this.loadarray(this.reqid);
    })
    
  }







  edit(ph:Product_Has_Request){
    localStorage.setItem("idorder",ph.idproducthasrequest.toString());
    this.router.navigate(["edit-car"]);
  }


}
