import { Component, OnInit } from '@angular/core';
import { ServiceProducthasrequestService } from 'src/app/Service/service-producthasrequest.service';
import { Router } from '@angular/router';
import { Product_Has_Request } from 'src/app/Model/Product_Has_Request';
import { ServiceProductService } from 'src/app/Service/service-product.service';
import { Product } from 'src/app/Model/Product';

@Component({
  selector: 'app-edit-car',
  templateUrl: './edit-car.component.html',
  styleUrls: ['./edit-car.component.css']
})
export class EditCarComponent implements OnInit {

  private ido: number;
  private allpro: Product[];
  private order:Product_Has_Request = new Product_Has_Request();
  private p:Product = new Product();
  private emptyMessage : string;
  private loginEmpty = false;
  private error = false;
  private errorMessage : string;
  constructor(private service:ServiceProducthasrequestService, private router:Router, private servicep:ServiceProductService) { }

  ngOnInit() {
    this.load();
  }

  load(){
    this.ido = +localStorage.getItem("idorder");
    this.service.getOrderId(this.ido)
    .subscribe(data=>{
      this.order = data;
    })

    this.servicep.getProducts()
    .subscribe(data=>{
      this.allpro = data;
    })
  }

  back(){
    this.router.navigate(["car-add-products"]);
  }




  update(o:Product_Has_Request){
    if(o.productidproduct>0 && o.producthasrequestamount>0){
      this.loginEmpty = false;
      this.p = this.findproduct(o.productidproduct);
      if(this.p!=null){
        this.error = false;
        o.total = this.p.productunitcost * o.producthasrequestamount;
        this.service.updateOrder(o)
        .subscribe(data=>{
          this.order = data;
          this.router.navigate(["car-add-products"]);
        })

      }else{
        this.error = true;
        this.errorMessage = "Non-existent product";
      }

    }else{
      this.loginEmpty = true;
      this.emptyMessage = 'Empty Fields';
    }
  }


  findproduct(id:number){
    this.p = null;
    for (let ind = 0; ind < this.allpro.length; ind++) {
      if(id == this.allpro[ind].idproduct){
        return this.allpro[ind];
        break;
      }}
  }
}
