import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServiceRequestService } from 'src/app/Service/service-request.service';
import { Request } from 'src/app/Model/Request';

@Component({
  selector: 'app-home-car',
  templateUrl: './home-car.component.html',
  styleUrls: ['./home-car.component.css']
})
export class HomeCarComponent implements OnInit {

 
  private idsell: number;
  private Requests: Request[];
  private reqid:number;
  constructor(private router:Router, private service:ServiceRequestService) { }

  ngOnInit() {
    let ids = localStorage.getItem("id");
    this.idsell = +ids;
    this.service.getRequestByIdSell(this.idsell)
    .subscribe(data=>{  
      this.Requests = data;
    })
  }

  back(){
    localStorage.setItem("id",this.idsell.toString());
    this.router.navigate(["main-sell"]);
  }


  prod(r:Request){
    localStorage.setItem("idre",r.idrequest.toString());
    this.router.navigate(["car-add-products"]);
  }


} 
